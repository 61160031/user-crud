
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Tramboliko
 */
public class User implements Serializable {
    private String loginName;
    private String password;
    
    public User(String loginName,String password){
        this.loginName = loginName;
        this.password = password;
    }

    public String getUser() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPass() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User username: " + loginName + ", password: " + password;
    }  
    
}

